<?php
  /**
   * Created by PhpStorm.
   * User: Anu
   * Date: 9/26/2018
   * Time: 1:20 PM
   */
?>
<!-- Footer -->
<section id="footer">
  <div class="container">
<!--    <div class="row text-center text-xs-center text-sm-left text-md-left justify-content-center">-->
<!--    </div>-->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 mt-1 mt-sm-2">
          <ul class="list-unstyled list-inline quick-links text-center">
            <li class="list-inline-item"><a href="javascript:void();"><i class=""></i>Home</a></li>
            <li class="list-inline-item"><a href="javascript:void();"><i class=""></i>My Account</a></li>
            <li class="list-inline-item"><a href="javascript:void();"><i class=""></i>Privacy Policy</a></li>
            <li class="list-inline-item"><a href="javascript:void();"><i class=""></i>Term and Conditions</a></li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2">
          <ul class="list-unstyled list-inline social text-center">
            <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-facebook"></i></a></li>
            <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-twitter"></i></a></li>
            <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-instagram"></i></a></li>
            <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-google-plus"></i></a></li>
          </ul>
        </div>
        </hr>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2">
          <ul class="list-unstyled list-inline text-center">
            <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-phone"></i>&nbsp +94 77 212
                1123</a></li>&nbsp &nbsp
            <li class="list-inline-item"><a href="javascript:void();" target="_blank"><i class="fa fa-envelope"></i>&nbsp
                scaflaka@gmail.lk
              </a></li>
          </ul>
        </div>
        </hr>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
          <p class="h6">&copy All right Reversed.</p>
          <p class="h6">Developed by :<a class="text-secondary ml-1" href="https://www.asianseye.com"
                                        target="_blank">Asians
              Eye</a></p>
        </div>
        </hr>
      </div>
    </div>
</section>
<!-- ./Footer -->

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
<script type="text/javascript">
        $(document).ready(function() {
  $('#table_cat_papers').DataTable();
});
      </script>
</body>
</html>
