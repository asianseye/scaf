<?php
  /**
   * Created by PhpStorm.
   * User: Anu
   * Date: 9/26/2018
   * Time: 10:33 AM
   */?>
			<nav class="navbar navbar-expand-lg nav fixed-top navbar-light nav nav-transparent">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item <?php if($this->uri->uri_string() == '') { echo 'active'; } ?>">
							<a class="nav-link nav-menu-title" href="<?php echo base_url();?>" style="font-size: medium">HOME <span
           class="sr-only">
           (current)</span></a>
						</li>
						<li class="nav-item <?php if($this->uri->uri_string() == 'exam') { echo 'active'; } ?>">
							<a class="nav-link nav-menu-title" href="<?php echo base_url();?>">EXAM PAPERS</a>
						</li>
						<li class="nav-item <?php if($this->uri->uri_string() == 'institue') { echo 'active'; } ?>">
							<a class="nav-link nav-menu-title" href="<?php echo base_url();?>">INSTITUTES</a>
						</li>
						<li class="nav-item <?php if($this->uri->uri_string() == 'home/news_event') { echo 'active'; } ?>">
							<a class="nav-link nav-menu-title" href="<?php echo base_url();
							?>home/news_event">TEACHERS</a>
						</li>
						<li class="nav-item <?php if($this->uri->uri_string() == 'registration') { echo 'active'; } ?>">
							<a class="nav-link nav-menu-title" href="<?php echo base_url();?>registration">CONTACT
                                US</a>
						</li>
                        <li class="nav-item <?php if($this->uri->uri_string() == 'registration') { echo 'active'; } ?>">
							<a class="nav-link nav-menu-title" href="<?php echo base_url();?>registration">LOGIN</a>
						</li>
                        <li class="nav-item <?php if($this->uri->uri_string() == 'registration') { echo 'active'; } ?>">
							<a class="nav-link nav-menu-title" href="<?php echo base_url();?>registration">REGISTER</a>
						</li>
					</ul>
				</div>
			</nav>

