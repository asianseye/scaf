<?php
  /**
   * Created by PhpStorm.
   * User: Anu
   * Date: 9/27/2018
   * Time: 4:15 PM
   */ ?>
<!DOCTYPE html>
<html>
<head>
  <title>Exam Papers</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/new_nav.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/exam.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/footer.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light nav-transparent">
  <a class="navbar-brand" href="#">SCAF.lk</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item <?php if ($this->uri->uri_string() == '') {
        echo 'active';
      } ?>">
        <a class="nav-link nav-menu-title" href="<?php echo base_url(); ?>" style="font-size: medium">HOME <span
                  class="sr-only">
           (current)</span></a>
      </li>
      <li class="nav-item <?php if ($this->uri->uri_string() == 'exam') {
        echo 'active';
      } ?>">
        <a class="nav-link nav-menu-title" href="<?php echo base_url(); ?>exam_papers">EXAM
          PAPERS</a>
      </li>
      <li class="nav-item <?php if ($this->uri->uri_string() == 'institue') {
        echo 'active';
      } ?>">
        <a class="nav-link nav-menu-title" href="<?php echo base_url(); ?>">INSTITUTES</a>
      </li>
      <li class="nav-item <?php if ($this->uri->uri_string() == 'home/news_event') {
        echo 'active';
      } ?>">
        <a class="nav-link nav-menu-title" href="<?php echo base_url();
        ?>home/news_event">TEACHERS</a>
      </li>
      <li class="nav-item <?php if ($this->uri->uri_string() == 'registration') {
        echo 'active';
      } ?>">
        <a class="nav-link nav-menu-title" href="<?php echo base_url(); ?>registration">CONTACT
          US</a>
      </li>
      <li class="nav-item <?php if ($this->uri->uri_string() == 'registration') {
        echo 'active';
      } ?>">
        <a class="nav-link nav-menu-title" href="<?php echo base_url(); ?>registration">LOGIN</a>
      </li>
      <li class="nav-item <?php if ($this->uri->uri_string() == 'registration') {
        echo 'active';
      } ?>">
        <a class="nav-link nav-menu-title" href="<?php echo base_url(); ?>registration">REGISTER</a>
      </li>
    </ul>
  </div>
</nav>
<div class="container mt-5">
  <div class="row">
    <?php
      foreach($results as $result) {
        $cat_id = $result['id_category'];
        $cat_name = $result['cat_name'];
    ?>
    <div class="col-4 mb-4 mt-4">
      <h3><a href="<?php echo base_url();?>papers_category/<?php echo $cat_id; ?>"><?php echo $cat_name;?></a></h3>
      <ul class="list-group list-group-flush exam-list">
        <?php foreach($result['tbl_subject'] as $subject){ ?>
        <li class="list-group-item d-flex justify-content-between align-items-center">
         <?php echo $subject['name'] ?><span class="badge badge-success">14</span></li>
        <?php } ?>
      </ul>
    </div>
    <?php } ?>
  </div>
</div>

<!-- #########  Footer section ######## -->
<?php include('partials/footer.php'); ?>
