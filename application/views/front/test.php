<?php
  /**
   * Created by PhpStorm.
   * User: Anu
   * Date: 9/26/2018
   * Time: 6:55 PM
   */ ?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css">
  <!--  <link rel="stylesheet" type="text/css"-->
  <!--        href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/test.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/footer.css">
</head>
<body>
<nav class="navbar navbar-expand-lg nav fixed-top navbar-light nav nav-transparent">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
          aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto" id="item">
      <li class="nav-item <?php if ($this->uri->uri_string() == '') {
        echo 'active';
      } ?>">
        <a class="nav-link nav-menu-title" href="<?php echo base_url(); ?>" style="font-size: medium">HOME <span
                  class="sr-only">
           (current)</span></a>
      </li>
      <li class="nav-item <?php if ($this->uri->uri_string() == 'exam') {
        echo 'active';
      } ?>">
        <a class="nav-link nav-menu-title" href="<?php echo base_url(); ?>exam_papers">EXAM
          PAPERS</a>
      </li>
      <li class="nav-item <?php if ($this->uri->uri_string() == 'institue') {
        echo 'active';
      } ?>">
        <a class="nav-link nav-menu-title" href="<?php echo base_url(); ?>">INSTITUTES</a>
      </li>
      <li class="nav-item <?php if ($this->uri->uri_string() == 'home/news_event') {
        echo 'active';
      } ?>">
        <a class="nav-link nav-menu-title" href="<?php echo base_url();
        ?>home/news_event">TEACHERS</a>
      </li>
      <li class="nav-item <?php if ($this->uri->uri_string() == 'registration') {
        echo 'active';
      } ?>">
        <a class="nav-link nav-menu-title" href="<?php echo base_url(); ?>registration">CONTACT
          US</a>
      </li>
      <li class="nav-item <?php if ($this->uri->uri_string() == 'registration') {
        echo 'active';
      } ?>">
        <a class="nav-link nav-menu-title" href="<?php echo base_url(); ?>registration">LOGIN</a>
      </li>
      <li class="nav-item <?php if ($this->uri->uri_string() == 'registration') {
        echo 'active';
      } ?>">
        <a class="nav-link nav-menu-title" href="<?php echo base_url(); ?>registration">REGISTER</a>
      </li>
    </ul>
  </div>
</nav>
<div class="container-fluid main-section mb-5">
  <div class="row">
    <div class="col-7 select-part-section p-2 rounded">
      <div class="row">
        <div class="col-9">
          <input type="text" class="form-control" placeholder="Seach your papers here" style="font-size: 19px;">
        </div>
        <div class="col-3">
          <a href="http://nicesnippets.com" target="_blank" class="btn btn-danger btn-block search-btn"><i
                    class="fa fa-search" aria-hidden="true"></i></a>
        </div>
      </div>
    </div>
  </div>
  <div class="row justify-content-center mt-4">
    <p class="lead text-center text-dark" style="font-size: 24px;">We have the largest collection of courses</p><br>
  </div>
  <div class="row justify-content-center">
    <a class="lead text-center text-dark" style="font-size: 20px;">View all courses &#187;</a>
  </div>
  <div class="container">
    <div class="row justify-content-center mt-5">
      <div class="col-3">
        <p class="lead text-center text-dark count">332</p>
        <hr width="100px;" class="text-light" style="color: #ffffff;">
        <p class="lead text-center text-dark count">Courses</p>
      </div>
      <div class="col-3">
        <p class="lead text-center text-dark count">4572</p>
        <hr width="100px;" class="text-light" style="color: #ffffff;">
        <p class="lead text-center text-dark count">Members</p>
      </div>
      <div class="col-3">
        <p class="lead text-center text-dark count">721</p>
        <hr width="100px;" class="text-light" style="color: #ffffff;">
        <p class="lead text-center text-dark count">Teachers</p>
      </div>
      <div class="col-3">
        <p class="lead text-center text-dark count">22</p>
        <hr width="100px;" class="text-light" style="color: #ffffff;">
        <p class="lead text-center text-dark count">Subjects</p>
      </div>
    </div>
  </div>
  <div class="container mt-5" style="margin-top: 1000px;">
    <div class="container mt-5 justify-content-center">
      <h2 class="text-center"><strong>WHAT IS SCAF.LK</strong></h2>
      <div class="row justify-content-center mt-5">
        <p class="lead">Scaf.lk is, a web based educational tool that enables students attempt MCQ exams online</p>
      </div>
    </div>
  </div>
  <!-- #########  Site info ######## -->
  <div class="container mt-5 mb-5">
    <div class="row">
      <div class="col-3">blah</div>
      <div class="col-3">blah</div>
      <div class="col-3">blah</div>
      <div class="col-3">blah</div>
    </div>
  </div>
  <!-- #########  Exam category section ######## -->
  <div class="container mt-5">
    <div class="row justify-content-center">
      <h2 class="text-center"><strong>EXAM CATEGORIES</strong></h2>
    </div>
    <div class="row mt-5">
      <div class="col-3">
        <div class="card">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/ol.jpg">
          <div class="card-body">
            <h5 class="card-title"><strong>5 Grade Exam</strong></h5>
          </div>
        </div>
      </div>
      <div class="col-3">
        <div class="card">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/ol.jpg">
          <div class="card-body">
            <h5 class="card-title"><strong>Odinary Level Exam</strong></h5>
          </div>
        </div>
      </div>
      <div class="col-3">
        <div class="card">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/ol.jpg">
          <div class="card-body">
            <h5 class="card-title"><strong>Advance Level Exam</strong></h5>
          </div>
        </div>
      </div>
      <div class="col-3">
        <div class="card">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/ol.jpg">
          <div class="card-body">
            <h5 class="card-title"><strong>Driving Licence Exam</strong></h5>
          </div>
        </div>
      </div>
    </div>
    <div class="row mt-5">
      <div class="col-3">
        <div class="card">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/ol.jpg">
          <div class="card-body">
            <h5 class="card-title"><strong>Law Entrace Exam</strong></h5>
          </div>
        </div>
      </div>
      <div class="col-3">
        <div class="card">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/ol.jpg">
          <div class="card-body">
            <h5 class="card-title"><strong>Bank Exam</strong></h5>
          </div>
        </div>
      </div>
      <div class="col-3">
        <div class="card">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/ol.jpg">
          <div class="card-body">
            <h5 class="card-title"><strong>Korean Exam</strong></h5>
          </div>
        </div>
      </div>
      <div class="col-3">
        <div class="card">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/ol.jpg">
          <div class="card-body">
            <h5 class="card-title"><strong>Administrative Exam</strong></h5>
          </div>
        </div>
      </div>
    </div>
    <!--  Testimonials  -->
    <div class="container">
      <div class="row">
        <div class="col-md-8 offset-md-2 col-10 offset-1 mt-5">
          <h2 class="text-center mt-5 mb-5 pb-2 text-uppercase text-dark"><strong>Testimonials</strong></h2>
          <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner mt-4">
              <div class="carousel-item text-center active">
                <div class="img-box p-1 border rounded-circle m-auto">
                  <img class="d-block w-100 rounded-circle" src="http://nicesnippets.com/demo/profile-1.jpg"
                       alt="First slide">
                </div>
                <h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Paul Mitchel</strong></h5>
                <h6 class="text-dark m-0">Web Developer</h6>
                <p class="m-0 pt-3 text-dark">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam
                  eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper
                  malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>
              </div>
              <div class="carousel-item text-center">
                <div class="img-box p-1 border rounded-circle m-auto">
                  <img class="d-block w-100 rounded-circle" src="http://nicesnippets.com/demo/profile-3.jpg"
                       alt="First slide">
                </div>
                <h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Steve Fonsi</strong></h5>
                <h6 class="text-dark m-0">Web Designer</h6>
                <p class="m-0 pt-3 text-dark">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam
                  eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper
                  malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>
              </div>
              <div class="carousel-item text-center">
                <div class="img-box p-1 border rounded-circle m-auto">
                  <img class="d-block w-100 rounded-circle" src="http://nicesnippets.com/demo/profile-7.jpg"
                       alt="First slide">
                </div>
                <h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Daniel vebar</strong></h5>
                <h6 class="text-dark m-0">Seo Analyst</h6>
                <p class="m-0 pt-3 text-dark">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam
                  eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper
                  malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- #########  Footer section ######## -->
  <?php include('partials/footer.php'); ?>
</div> <!-- /main section -->

