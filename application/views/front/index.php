<?php
  /**
   * Created by PhpStorm.
   * User: Anu
   * Date: 9/26/2018
   * Time: 10:29 AM
   */
  // include header
  include('partials/navigation.php');
?>
  <!-- #########  Header Section ######## -->
<!--  <div class="mt-5">-->
<!--    <img src="--><?php //echo base_url(); ?><!--assets/img/fullscreen-slider.jpg" alt="" style="width: 100%">-->
<!--    <div class="search form-group">-->
<!--      <input type="text" class="depth form-control" id="saveServer" placeholder="Search for anything"/>-->
<!--      <button type="submit" class="btn btn-light" value="Search">Search</button>-->
<!--      <h4 class=" mt-3">We have the largest collection of exam papers</h4>-->
<!--      <h5 class="text-primary mt-3">View all Papers</h5>-->
<!--    </div>-->
<!--  </div>-->
  	<div class="container-fluid main-section">
		<div class="row">
    <img src="--><?php echo base_url(); ?>assets/img/fullscreen-slider.jpg" alt="" style="width: 100%">
			<div class="col-12 text-white text-center mb-5">
				<h1> Lets Us Search </h1>
			</div>
			<div class="col-7 select-part-section p-2 rounded">
				<div class="row">
					<div class="col-9">
       <input type="text" class="form-control">
					</div>
					<div class="col-3">
						<a href="http://nicesnippets.com" target="_blank" class="btn btn-danger btn-block search-btn"><i class="fa fa-search" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
  <!-- #########  What is section ######## -->
  <div class="container">
  <div class="container mt-5 justify-content-center">
      <h1 class="text-center">WHAT IS SCAF.LK?</h1>
      <hr width="200px;">
      <div class="row justify-content-center">
        <p class="lead">Scaf.lk is, a web based educational tool that enables students attempt MCQ exams online</p>
      </div>
  </div>

  <!-- #########  Site info ######## -->
  <div class="container mt-5 mb-5">
    <div class="row">
      <div class="col-3">blah</div>
      <div class="col-3">blah</div>
      <div class="col-3">blah</div>
      <div class="col-3">blah</div>
    </div>
  </div>
  <!-- #########  Exam category section ######## -->
  <div class="card" style="padding-left: 10px; padding-right: 10px; padding-bottom: 80px;">
  <div class="container mt-5">
    <div class="row justify-content-center">
      <h1 class="text-center">EXAM CATEGORY</h1>
    </div>
    <hr width="200px;">
    <div class="row mt-5">
      <div class="col-3">
        <div class="card">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/ol.jpg">
          <div class="card-body">
            <h5 class="card-title"><strong>5 Grade Exam</strong> </h5>
          </div>
        </div>
      </div>
      <div class="col-3">
        <div class="card">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/ol.jpg">
          <div class="card-body">
            <h5 class="card-title"><strong>Odinary Level Exam</strong> </h5>
          </div>
        </div>
      </div>
      <div class="col-3">
        <div class="card">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/ol.jpg">
          <div class="card-body">
            <h5 class="card-title"><strong>Advance Level Exam</strong> </h5>
          </div>
        </div>
      </div>
      <div class="col-3">
        <div class="card">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/ol.jpg">
          <div class="card-body">
            <h5 class="card-title"><strong>Driving Licence Exam</strong> </h5>
          </div>
        </div>
      </div>
    </div>
    <div class="row mt-5">
      <div class="col-3">
        <div class="card">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/ol.jpg">
          <div class="card-body">
            <h5 class="card-title"><strong>Law Entrace Exam</strong> </h5>
          </div>
        </div>
      </div>
      <div class="col-3">
        <div class="card">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/ol.jpg">
          <div class="card-body">
            <h5 class="card-title"><strong>Bank Exam</strong> </h5>
          </div>
        </div>
      </div>
      <div class="col-3">
        <div class="card">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/ol.jpg">
          <div class="card-body">
            <h5 class="card-title"><strong>Korean Exam</strong> </h5>
          </div>
        </div>
      </div>
      <div class="col-3">
        <div class="card">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/ol.jpg">
          <div class="card-body">
            <h5 class="card-title"><strong>Administrative Exam</strong> </h5>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  <!-- #########  What we for section ######## -->
  <div class="container mt-5">
    <div class="row justify-content-center">
      <h1 class="text-center">WHAT WE FOR?</h1>
    </div>
    <div class="row justify-content-center mt-2">
      <div class="col-4">
        <div class="card">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/student.jpg" style="height: 30%">
        </div>
      </div>
      <div class="col-4">
        <div class="card">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/teacher.jpg">
        </div>
      </div>
      <div class="col-4">
        <div class="card">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/institute2.jpg">
        </div>
      </div>
    </div>
  </div>
<!--  Testimonials  -->
<div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2 col-10 offset-1 mt-5">
                    <h2 class="text-center mt-5 mb-5 pb-2 text-uppercase text-dark"><strong>Testimonials</strong></h2>
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                      <div class="carousel-inner mt-4">
                        <div class="carousel-item text-center active">
                            <div class="img-box p-1 border rounded-circle m-auto">
                                <img class="d-block w-100 rounded-circle" src="http://nicesnippets.com/demo/profile-1.jpg" alt="First slide">
                            </div>
                            <h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Paul Mitchel</strong></h5>
                            <h6 class="text-dark m-0">Web Developer</h6>
                            <p class="m-0 pt-3 text-dark">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam
                              eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>
                        </div>
                        <div class="carousel-item text-center">
                            <div class="img-box p-1 border rounded-circle m-auto">
                                <img class="d-block w-100 rounded-circle" src="http://nicesnippets.com/demo/profile-3.jpg" alt="First slide">
                            </div>
                            <h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Steve Fonsi</strong></h5>
                            <h6 class="text-dark m-0">Web Designer</h6>
                            <p class="m-0 pt-3 text-dark">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam
                              eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>
                        </div>
                        <div class="carousel-item text-center">
                            <div class="img-box p-1 border rounded-circle m-auto">
                                <img class="d-block w-100 rounded-circle" src="http://nicesnippets.com/demo/profile-7.jpg" alt="First slide">
                            </div>
                            <h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Daniel vebar</strong></h5>
                            <h6 class="text-dark m-0">Seo Analyst</h6>
                            <p class="m-0 pt-3 text-dark">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam
                              eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
  </div>
  <!-- #########  Footer section ######## -->
<?php include('partials/footer.php'); ?>