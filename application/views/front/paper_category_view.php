<?php
  /**
   * Created by PhpStorm.
   * User: Anu
   * Date: 9/30/2018
   * Time: 10:14 AM
   */ ?>
<!DOCTYPE html>
<html>
<head>
  <title>Exam Papers</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
<!--  <link rel="stylesheet" href="--><?php //echo base_url(); ?><!--assets/DataTables/datatables.min.css">-->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/new_nav.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/exam.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/footer.css">

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">



</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light nav-transparent">
  <a class="navbar-brand" href="#">SCAF.lk</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item <?php if ($this->uri->uri_string() == '') {
        echo 'active';
      } ?>">
        <a class="nav-link nav-menu-title" href="<?php echo base_url(); ?>" style="font-size: medium">HOME <span
                  class="sr-only">
           (current)</span></a>
      </li>
      <li class="nav-item <?php if ($this->uri->uri_string() == 'exam') {
        echo 'active';
      } ?>">
        <a class="nav-link nav-menu-title" href="<?php echo base_url(); ?>exam_papers">EXAM
          PAPERS</a>
      </li>
      <li class="nav-item <?php if ($this->uri->uri_string() == 'institue') {
        echo 'active';
      } ?>">
        <a class="nav-link nav-menu-title" href="<?php echo base_url(); ?>">INSTITUTES</a>
      </li>
      <li class="nav-item <?php if ($this->uri->uri_string() == 'home/news_event') {
        echo 'active';
      } ?>">
        <a class="nav-link nav-menu-title" href="<?php echo base_url();
        ?>home/news_event">TEACHERS</a>
      </li>
      <li class="nav-item <?php if ($this->uri->uri_string() == 'registration') {
        echo 'active';
      } ?>">
        <a class="nav-link nav-menu-title" href="<?php echo base_url(); ?>registration">CONTACT
          US</a>
      </li>
      <li class="nav-item <?php if ($this->uri->uri_string() == 'registration') {
        echo 'active';
      } ?>">
        <a class="nav-link nav-menu-title" href="<?php echo base_url(); ?>registration">LOGIN</a>
      </li>
      <li class="nav-item <?php if ($this->uri->uri_string() == 'registration') {
        echo 'active';
      } ?>">
        <a class="nav-link nav-menu-title" href="<?php echo base_url(); ?>registration">REGISTER</a>
      </li>
    </ul>
  </div>
</nav>
<div class="container mt-5">
  <h2><?php
      if (!empty($category_name[0]['ct_name'])){
        echo $category_name[0]['ct_name'];
      }?>
  </h2>

  <div class="row mt-5">
    <div class="col-12">
      <table class="table table-hover" id="table_cat_papers">
        <thead>
        <tr>
          <th scope="col">Teacher</th>
          <th scope="col">Teacher Name</th>
          <th scope="col">Category</th>
          <th scope="col">Subject</th>
          <th scope="col">Paper</th>
          <th scope="col">Duration</th>
          <th scope="col">Type</th>
          <th scope="col">Price</th>
          <th scope="col">Take Test</th>
        </tr>
        </thead>
        <tbody>
        <?php if(!empty($papers)){
          foreach ($papers as $i => $paper){?>
            <?php
              $paper_id = $paper['id_paper'];
              $paper_cat = $paper['cat_name'];
              $paper_auth = $paper['auth_name'];
              $paper_sub_name = $paper['sub_name'];
              $paper_name = $paper['paper_name'];
              $paper_desc = $paper['description'];
              $paper_time = $paper['time'];
              $paper_type = $paper['type'];
              $paper_price = $paper['price'];
            ?>
            <!-- Modal -->
            <div class="modal fade" id="paper<?php echo $paper_id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><?php echo $paper_cat; ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <p><strong>Subject</strong> : <?php echo $paper_sub_name; ?></p>
                    <p><strong>Paper Title</strong> : <?php echo $paper_name; ?></p>
                    <p><strong>Description</strong> : <?php echo $paper_desc; ?></p>
                    <p><strong>Total Questions</strong> : <?php echo "Not configure yet"; ?></p>
                    <p><strong>Duration (Min)</strong> : <?php echo "Not configure yet"; ?></p>
                    <p><strong>Amount</strong> : <?php echo $paper_price; ?></p>
                    <p><strong>Teacher</strong> : <?php echo $paper_auth; ?></p>
                  </div>
                  <div class="modal-footer">
                    <a href="<?php echo base_url(); ?>test/<?php echo $paper_id; ?>" class="btn btn-dark">START NOW</a>
<!--                    <button type="button" class="btn btn-dark">START NOW</button>-->
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                 </div>
                </div>
              </div>
            </div>
            <!--modal-->
            <tr>
              <th><?php echo $paper_auth; ?></th>
              <td><?php echo $paper_auth; ?></td>
              <td><?php echo $paper_cat; ?></td>
              <td><?php echo $paper_sub_name; ?></td>
              <td class="col-sm-2"><?php echo $paper_name; ?></td>
              <td><?php echo $paper_time; ?></td>
              <td><?php echo $paper_type; ?></td>
              <td><?php echo $paper_price; ?></td>
              <td>
                <button type="button" data-toggle="modal" data-target="#paper<?php echo $paper_id; ?>"
                          class="btn btn-primary">Take Test</button>
              </td>
            </tr>
          <?php } } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<!-- #########  Footer section ######## -->
<?php
  /**
   * Created by PhpStorm.
   * User: Anu
   * Date: 9/26/2018
   * Time: 1:20 PM
   */
?>
<!-- Footer -->
<section id="footer">
  <div class="container">
<!--    <div class="row text-center text-xs-center text-sm-left text-md-left justify-content-center">-->
<!--    </div>-->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 mt-1 mt-sm-2">
          <ul class="list-unstyled list-inline quick-links text-center">
            <li class="list-inline-item"><a href="javascript:void();"><i class=""></i>Home</a></li>
            <li class="list-inline-item"><a href="javascript:void();"><i class=""></i>My Account</a></li>
            <li class="list-inline-item"><a href="javascript:void();"><i class=""></i>Privacy Policy</a></li>
            <li class="list-inline-item"><a href="javascript:void();"><i class=""></i>Term and Conditions</a></li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2">
          <ul class="list-unstyled list-inline social text-center">
            <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-facebook"></i></a></li>
            <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-twitter"></i></a></li>
            <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-instagram"></i></a></li>
            <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-google-plus"></i></a></li>
          </ul>
        </div>
        </hr>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2">
          <ul class="list-unstyled list-inline text-center">
            <li class="list-inline-item"><a href="javascript:void();"><i class="fa fa-phone"></i>&nbsp +94 77 212
                1123</a></li>&nbsp &nbsp
            <li class="list-inline-item"><a href="javascript:void();" target="_blank"><i class="fa fa-envelope"></i>&nbsp
                scaflaka@gmail.lk
              </a></li>
          </ul>
        </div>
        </hr>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
          <p class="h6">&copy All right Reversed.</p>
          <p class="h6">Developed by :<a class="text-secondary ml-1" href="https://www.asianseye.com"
                                        target="_blank">Asians
              Eye</a></p>
        </div>
        </hr>
      </div>
    </div>
</section>
<!-- ./Footer -->
<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#table_cat_papers').DataTable();
    });
</script>
</body>
</html>