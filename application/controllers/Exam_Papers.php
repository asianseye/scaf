<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  /**
   * Created by PhpStorm.
   * User: Anu
   * Date: 9/27/2018
   * Time: 4:21 PM
   */

  class Exam_Papers extends CI_Controller
  {
    private $data;
     public function __construct(){
        parent::__construct();
     }
    public function index()
	   {
	     $this->data['results'] = $this->ExamPapersModel->getAllExamPapers();
		    $this->load->view('front/exam_papers_view',$this->data);
	   }
  }