<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  class Papers_Category  extends CI_Controller {
    // To order run index method with params we need implement _remap($param) method
  	function _remap($param) {
        $this->index($param);
    }

    function index($param){
        $data['papers'] = $this->Paper_Category->getPaper($param);
        $data['category_name'] = $this->Paper_Category->getCategoryNameById($param);
        $this->load->view('front/paper_category_view',$data);
    }

  }

  /* End of file Controllername.php */