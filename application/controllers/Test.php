<?php
  /**
   * Created by PhpStorm.
   * User: Anu
   * Date: 10/3/2018
   * Time: 2:50 PM
   */
  defined('BASEPATH') OR exit('No direct script access allowed');

  class Test extends CI_Controller {

    private $results;
    public function __construct(){
        parent::__construct();
    }

    function _remap($param) {
        $this->index($param);
    }

  	 public function index($param)
  	 {
  	   // Get 1st question and answer
  	   $this->results['question'] = $this->Test_Model->getPaperFirstQuestions($param);
  	   // Get paper details from paper id
  	   $paper_details = $this->results['paper_details'] = $this->getPaperDetails($param);

  	   // Get number of question of the paper
      $paper_question_count = $this->Test_Model->getQuestionCount($param);
  	   // Load test_paper_view with paper details and with 1st question
  	   $this->load->view('front/test_paper_view',$this->results);
  	   // Set paper details to session
      $this->session->set_userdata('paper_details',$paper_details);
      $this->session->set_userdata('question_count',$paper_question_count);
  	 }

  	 public function nextQuestion($previousQuestionID){
    echo $previousQuestionID;
//      $this->load->view('front/test_paper_view',$this->results);
    }
    // get Paper Details
    public function getPaperDetails($param){
      $row =  $this->Test_Model->getPaperDetails($param);
      return $row;
    }

    public function getNextQuestionByID($id){
     $row = $this->Test_Model->getDetailsOfQuestion($id+1);
    }

  }

  /* End of file Test.php */