<?php
  /**
   * Created by PhpStorm.
   * User: Anu
   * Date: 9/27/2018
   * Time: 4:12 PM
   */

  class ExamPapersModel extends CI_Model
  {
//    This is the method i just try to archive
    public function getAllExamPapers()
    {
      // get distinct item of category table and join table with subject table
      $this->db->distinct();
      $this->db->select('tbl_category.id_category,tbl_category.name AS cat_name');
      $this->db->join('tbl_subject', 'tbl_category.id_category = tbl_subject.id_category_fk');
      $this->db->join('tbl_paper', 'tbl_subject.id_subject = tbl_paper.id_subject_fk');
      $results = $this->db->get('tbl_category')->result_array();

      // Loop through the tbl_category array
      foreach ($results as $i => $result) {
        // Get an array of subject
        // 'id_category_fk' is the foreign_key in the tbl_subject table
        $this->db->where('id_category_fk', $result['id_category']);
        $sub_query = $this->db->get('tbl_subject')->result_array();

        // Add the subject array to the array entry for this category
         $results[$i]['tbl_subject'] = $sub_query;
        // Loop through the tbl_subject array
        foreach ($sub_query as $s => $sub){
          $sub_id = $sub['id_subject'];
          // Get array of papers
          // 'id_subject_fk' is the foreign_key in the tbl_paper table
          $query = $this->db->query("SELECT COUNT('id_paper') AS 'numberOfPapers' FROM tbl_paper WHERE id_subject_fk = '$sub_id'");
//          $this->db->where('id_subject_fk', $sub['id_subject']);
//          $paper_query = $this->db->get('tbl_paper')->result_array();
          //$r = $query->num_rows();
          $sub_query['tbl_subject'][$s] =  $query;
          //$results[$i]['tbl_subject'] = $row->count;
//              return $row->count; // return the count

        }

      }
      return $results;
    }

  }