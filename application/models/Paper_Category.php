<?php
  /**
   * Created by PhpStorm.
   * User: Anu
   * Date: 9/30/2018
   * Time: 10:32 AM
   */
  defined('BASEPATH') OR exit('No direct script access allowed');

  class Paper_Category extends CI_Model
  {
    // get category, subject, paper and author details according to category id which catch from the URI ex
    // (papers_category/15)
    public function getPaper($cat_id){

      $this->db->select('tbl_category.id_category,tbl_category.name AS cat_name,tbl_subject.id_subject, tbl_subject.name AS sub_name,tbl_paper.id_paper, tbl_paper.name AS paper_name, tbl_paper.description, tbl_paper.time, tbl_paper.type, tbl_paper.price,tbl_author.id_author, tbl_author.name AS auth_name');
      $this->db->join('tbl_subject', 'tbl_category.id_category = tbl_subject.id_category_fk');
      $this->db->join('tbl_paper', 'tbl_subject.id_subject = tbl_paper.id_subject_fk');
      $this->db->join('tbl_author', 'tbl_author.id_author = tbl_paper.id_author_fk');
      $this->db->where('id_category', $cat_id);

      $results = $this->db->get('tbl_category')->result_array();
      return $results;
    }

    // get exact category name which catch from the URI ex (papers_category/15)
    public function getCategoryNameById($cat_id){
      $this->db->select('tbl_category.name AS ct_name');
      $this->db->where('id_category', $cat_id);
      $results = $this->db->get('tbl_category')->result_array();
      return $results;
    }

  }

  /* End of file Paper_Category.php */