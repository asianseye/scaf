<?php
  /**
   * Created by PhpStorm.
   * User: Anu
   * Date: 10/3/2018
   * Time: 3:00 PM
   */

  defined('BASEPATH') OR exit('No direct script access allowed');

  class Test_Model extends CI_Model
  {
    public function getAllPaperQuestions($paperID){
      $this->db->where('id_paper_fk', $paperID);
      $results = $this->db->get('tbl_question')->result_array();
      return $results;
    }
    public function getPaperFirstQuestions($paperID){
      $this->db->where('id_paper_fk', $paperID);
      $this->db->limit(1);
      $results = $this->db->get('tbl_question')->row_array();
      return $results;
    }
    // get paper details from paper ID
    public function getPaperDetails($paperID){
      $this->db->where('id_paper',$paperID);
      $results = $this->db->get('tbl_paper')->row_array();
      return $results;
    }
    // Get question information by ID
    public function getDetailsOfQuestion($questionID){
      $this->db->where('id_question',$questionID);
      $result = $this->db->get('tbl_question')->row_array();
      return $result;
    }

    // Get question count of paper (paper id)
    public function getQuestionCount($paperID){
//      $result = $this->db->where('id_paper_fk', $paperID)->count_all_results('id_paper_fk');
     // $this->output->enable_profiler(TRUE);

        $this->db->select('id_paper_fk');
        $this->db->where('id_paper_fk',$paperID);
        $this->db->from("tbl_question");
        $result = $this->db->get()->result();

      $c = count($result);
      return $c;

    }

  }

  /* End of file .php */